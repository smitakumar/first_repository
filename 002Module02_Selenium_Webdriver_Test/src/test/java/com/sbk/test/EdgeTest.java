/**
 * 
 */
package com.sbk.test;

/**
 * @author Smita B Kumar
 *
 */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.Test;

public class EdgeTest {
	//String edgeDriverPathValue="E:\\Selenium\\Software\\MicrosoftDriver\\MicrosoftWebDriver.exe";
//	String edgeDriverKey="webdriver.edge.driver"
	String driverPath = "E:\\Selenium\\Software\\MicrosoftDriver\\";
	public WebDriver driver;
	
	@Test(priority=1)
	public void launchBrowser() {
		System.out.println("launching Microsoft Edge browser");
		System.setProperty("webdriver.edge.driver", driverPath+"MicrosoftWebDriver.exe");
		driver = new EdgeDriver();
	}

	@Test(priority=2)
	public void openEdgeBrowser() {
		driver.navigate().to("http://www.google.com");
	}
	
	@Test(priority=3)
	public void closeDriver() {
		if(driver!=null) {
			driver.close();
		}
	}	
}