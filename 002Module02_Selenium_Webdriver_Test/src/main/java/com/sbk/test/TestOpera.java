package com.sbk.test;

import org.openqa.selenium.WebDriver;

import com.sbk.util.NightlyFirefoxUtil;
import com.sbk.util.OperaUtil;

/**
 * @author Smita
 *
 */
public class TestOpera {

	public static void main(String[] args) {
		WebDriver driver = OperaUtil.getDriver();
		System.out.println("launching Opera browser....");
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(160, TimeUnit.SECONDS);
		System.out.println("Opening Google page.....");
		driver.get("http://www.google.com");
		System.out.println("Title " + driver.getTitle());
		//if(driver!=null) { driver.close(); }
		 
	}

}
