/**
 * 
 */
package com.sbk.test.opera;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.opera.OperaDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import junit.framework.TestCase;

@RunWith(JUnit4.class)
public class OperaTest extends TestCase {

  private static OperaDriverService service;
  private WebDriver driver;

  @BeforeClass
  public static void createAndStartService() {
	  
    service = new OperaDriverService.Builder()
        .usingDriverExecutable(new File("driver\\operadriver.exe"))
        .usingAnyFreePort()
        .build();
    try {
		service.start();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }

  @AfterClass
  public static void createAndStopService() {
    service.stop();
  }

  @Before
  public void createDriver() {
    driver = new RemoteWebDriver(service.getUrl(),
        DesiredCapabilities.operaBlink());
  }

  @After
  public void quitDriver() {
    driver.quit();
  }

  @Test
  public void testGoogleSearch() throws InterruptedException {
    driver.get("http://www.google.com");
    WebElement searchBox = driver.findElement(By.name("q"));
    Thread.sleep(5000);
    searchBox.sendKeys("webdriver");
    searchBox.clear();
    assertEquals("webdriver - Google Search", driver.getTitle());
    
  }
}

//Note that unlike OperaDriver, RemoteWebDriver doesn't directly implement role interfaces such as LocationContext and WebStorage. Therefore, to access that functionality, it needs to be augmented and then cast to the appropriate interface.