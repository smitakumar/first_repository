/**
 * 
 */
package com.sbk.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * @author Smita B Kumar
 *
 */
public class ChromeDriverUtil {
	public static WebDriver getDriver(){
		WebDriver driver;
		String chromeDriverKey="webdriver.chrome.driver";
		String chromeDriverPathValue="driver\\chromedriver.exe";
		
		System.setProperty(chromeDriverKey, chromeDriverPathValue);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("test-type");
		options.addArguments("start-maximized");
		/*options.addArguments("--js-flags=--expose-gc");  
		options.addArguments("--enable-precise-memory-info"); 
		options.addArguments("--disable-popup-blocking");
		options.addArguments("--disable-default-apps");
		options.addArguments("test-type=browser");
		options.addArguments("disable-infobars");*/
		driver = new ChromeDriver(options);
		return driver;
	}
	public static void main(String[] args) {
		WebDriver driver=getDriver();
		System.out.println("launching Chrome browser....");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(160, TimeUnit.SECONDS);
		System.out.println("Opening Google page.....");
		driver.get("http://www.google.com");
		System.out.println("Title " + driver.getTitle());
	}
}
