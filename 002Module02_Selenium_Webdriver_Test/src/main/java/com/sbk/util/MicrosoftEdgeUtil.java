/**
 * 
 */
package com.sbk.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

/**
 * @author Smita B Kumar
 *
 */
public class MicrosoftEdgeUtil {
	public static WebDriver getDriver(){
		WebDriver driver;
		String edgeDriverPathValue="driver\\MicrosoftWebDriver.exe";
		String edgeDriverKey="webdriver.edge.driver";
		System.out.println("launching Microsoft Edge browser");
		System.setProperty(edgeDriverKey, edgeDriverPathValue);
		driver = new EdgeDriver();
		return driver;
	}
}
