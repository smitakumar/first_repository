/**
 * 
 */
package com.sbk.util;

import java.io.File;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * @author Smita B Kumar
 *
 */
public class OperaUtil {
	public static WebDriver getDriver() {
		WebDriver driver;
		/*
		 * System.setProperty("webdriver.opera.driver", "driver\\operadriver.exe");
		 * DesiredCapabilities capabilities = DesiredCapabilities.operaBlink();
		 * //capabilities.setCapability("webdriver.opera.driver",
		 * " driver\\operadriver.exe"); driver=new OperaDriver();
		 * getDriver().manage().window().setSize( new Dimension(1366, 768));
		 */
		String path = "driver\\operadriver.exe";
		OperaOptions options = new OperaOptions();
		options.setBinary("C:\\Program Files\\Opera\\57.0.3098.106\\opera.exe");
		System.setProperty("webdriver.opera.driver", path);
		driver = new OperaDriver(options);

		return driver;
	}
}
