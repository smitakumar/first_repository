/**
 * 
 */
package com.smita.webdriver09.image;

import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;		
import org.openqa.selenium.chrome.ChromeDriver;

import com.smita.webdriver.util.ChromeDriverUtil;		
/**
 * @author Smita
 *
 */
public class AccessingImageLink {
	    		
	    public static void main(String[] args) {									
	        String baseUrl = "https://www.facebook.com/login/identify?ctx=recover";					
	        System.setProperty("webdriver.chrome.driver","driver\\chromedriver.exe");					
	        WebDriver driver = ChromeDriverUtil.getDriver();					
	        		
	        driver.get(baseUrl);					
	        //click on the "Facebook" logo on the upper left portion//<title id="pageTitle">Facebook � log in or sign up</title>	
	        
				driver.findElement(By.cssSelector("a[title=\"Go to Facebook home\"]")).click();					

				//verify that we are now back on Facebook's homepage	
				String titile=driver.getTitle();
				System.out.println("Title : "+titile);
				if (titile.equals("Facebook � log in or sign up")) {							
	            System.out.println("We are back at Facebook's homepage");					
	        } else {			
	            System.out.println("We are NOT in Facebook's homepage");					
	        }		
					driver.close();		

	    }		
	}