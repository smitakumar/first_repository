/**
 * 
 */
package com.smita.webdriver08.capcha;

/**
 * @author brije
 *
 */

import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smita.webdriver.util.ChromeDriverUtil;
import com.smita.webdriver.util.FirefoxUtil;
public class CaptchaValidation {
	//Declaring variables
			private WebDriver driver; 
			private String baseUrl;

			@Before
			public void setUp() throws Exception{
				// Selenium version 3 beta releases require system property set up
				//System.setProperty("webdriver.gecko.driver", "driver\\geckodriver.exe");
				// Create a new instance for the class FirefoxDriver
				// that implements WebDriver interface
				driver = ChromeDriverUtil.getDriver();//new FirefoxDriver();
				// Implicit wait for 5 seconds
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				// Assign the URL to be invoked to a String variable
				//baseUrl = "https://www.in.ckgs.us";
				baseUrl="https://www.in.ckgs.us/";
				
			}
			@Test
			public void testPopUp() throws Exception{
				driver.get(baseUrl);
				
				System.out.println("Opening .... "+baseUrl);
				Thread.sleep(100);

				for (String winhandle: driver.getWindowHandles()) {
				    driver.switchTo().window(winhandle);
				    System.out.println("Closing Popup");        
				    Thread.sleep(100);
				    driver.findElement(By.xpath("//*[@id=\"roadblock\"]/button")).click();
				    System.out.println("Window Switch");  
				    driver.manage().window().maximize();
					driver.findElement(By.xpath("/html/body/div[2]/header/div/div/div[6]/div/nav/ul[2]/li/a")).click();					
					System.out.println("Opening .... "+baseUrl+"/myaccount/");
					Thread.sleep(100);
					// Locate 'Current Passport Number' text box by cssSelector: tag and name attribute
					WebElement passportNo = driver.findElement(By.cssSelector("input[name='currentPassportNo']"));
					// Clear the default placeholder or any value present
					passportNo.clear();
					// Enter/type the value to the text box
					passportNo.sendKeys("123456789");System.out.println("Entering passport Number in the textbox.... ");
					// Hit on the Login Button
					driver.findElement(By.xpath("//*[@id=\"btnSubmitMyAccount\"]")).click();
					//prompting user to enter captcha			
					String captchaVal = JOptionPane.showInputDialog("Please enter the captcha value:");
					//Type the entered captcha to the text box
		driver.findElement(By.id("recaptcha_response_field")).sendKeys(captchaVal);		

		System.out.println("Typing the entered captcha to the text box .... ");
				}
			}
			
			
			@Test
			public void testPageTitle() throws Exception{
				
			}
			
			 @After
			  public void tearDown() throws Exception{
				// Close the Firefox browser
				driver.close();
			}
	}