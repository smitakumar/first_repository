/**
 * 
 */
package com.smita.webdriver07.send_attachment_gmail;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.smita.webdriver.util.ChromeDriverUtil;

/**
 * @author Smita B Kumar
 *
 */
public class SendAttachmentFileViaGmail {
	public static void main(String[] args) throws InterruptedException, AWTException {
		/*
		 * Scenario:

		Open gmail.com
		Enter username 
		Enter Password
		Click on SignIn
		Click on composeButton
		Add recipent
		Add Subject
		Click on attachment
		Add your File location
		Click on Send Button
		 */
		WebDriver driver=ChromeDriverUtil.getDriver();
		System.out.println("Loading chrome brower....");
		driver.manage().window().maximize();
		//delete all cookies
		driver.manage().deleteAllCookies();
		System.out.println("Deleting all cookies....");
		driver.get("http://www.gmail.com/");
		System.out.println("opening Gmail....");
		//open gmail
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//Email //or //identifierId
		System.out.println("Entering username or email....");
		driver.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys("smitaselenium3@gmail.com");
		
		//enter emailid
		//next or  //identifierNext
		System.out.println("clicking next....");
		driver.findElement(By.xpath("//*[@id=\"identifierNext\"]/content/span")).click();
		//click on next
		Thread.sleep(1500);
		System.out.println("Entering password....");
		//Passwd or @name='password'
		driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).sendKeys("pihusia@0507");
		// enter password
		//passwordNext or signIn
		System.out.println("clicking next....");
		driver.findElement(By.xpath("//*[@id=\"passwordNext\"]/content/span")).click();
		Thread.sleep(1500);
		//click on sign in
		System.out.println("clicking on compose mail....");
		driver.findElement(By.xpath("//*[@id=\":5r\"]/div/div")).click();
		//click on compose button
		System.out.println("entering send-to email id....");
		driver.findElement(By.xpath("//form[1]//textarea[1]")).sendKeys("smitaselenium3@gmail.com");
		//enter email id where you need to send email
		System.out.println("entering subject....");
		driver.findElement(By.xpath("//div[@class='aoD az6']//input[@class='aoT']")).sendKeys("Please find attachment");
		//Enter subject
		Thread.sleep(1500);
		System.out.println("click on attachment icon....");
		driver.findElement(By.xpath("//div[@class='a1 aaA aMZ']")).click();
		//click on attachment icon
		StringSelection ss = new StringSelection("C:\\Program Files\\Java\\jdk1.8\\README.html");
		//upload your file using RobotClass
		//attach your path where file is located.
		System.out.println("attach your path where file is located....");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		Thread.sleep(5000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(6000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		System.out.println("Click on send....");
		driver.findElement(By.xpath("//div[text()='Send']")).click();
		//Click on send
	}
}
