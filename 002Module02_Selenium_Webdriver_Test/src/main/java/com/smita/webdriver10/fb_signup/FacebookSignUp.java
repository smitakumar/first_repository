/**
 * 
 */
package com.smita.webdriver10.fb_signup;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.smita.webdriver.util.ChromeDriverUtil;

/**
 * @author Smita
 *
 */
public class FacebookSignUp {

	public static void main(String[] args) throws InterruptedException {
		String baseUrl = "https://www.facebook.com/";
		WebDriver driver = ChromeDriverUtil.getDriver();
		driver.get(baseUrl);
		String titile = driver.getTitle();
		System.out.println("Title : " + titile);
		System.out.println("Entering Facebook Signup details .... ");
		System.out.println("Entering  firstname.... ");
		driver.findElement(By.name("firstname")).sendKeys("smita");
		System.out.println("Entering  lastname.... ");
		driver.findElement(By.name("lastname")).sendKeys("kumar");
		System.out.println("Entering  email.... ");
		driver.findElement(By.name("reg_email__")).sendKeys("sia.shivansh@gmail.com");
		// re-enter email
		System.out.println("Entering confirmed email.... ");
		driver.findElement(By.name("reg_email_confirmation__")).sendKeys("sia.shivansh@gmail.com");//
		System.out.println("Entering  new password.... ");
		driver.findElement(By.name("reg_passwd__")).sendKeys("pihusia@0507");

		// select the third dropdown using "select by index"
		System.out.println("Selecting  DOB ....day ");
		Select selectByIndex = new Select(driver.findElement(By.id("day")));
		selectByIndex.selectByIndex(10);
		Thread.sleep(500);
		System.out.println("Selecting  DOB ....month ");
		// select the second dropdown using "select by visible text"
		Select selectByValue = new Select(driver.findElement(By.id("month")));
		selectByValue.selectByVisibleText("Nov");
		Thread.sleep(500);
		System.out.println("Selecting  DOB ....year ");
		// select the first operator using "select by value"
		Select selectByVisibleText = new Select(driver.findElement(By.id("year")));
		selectByVisibleText.selectByValue("1980");

		// Select the deselected Radio button (female) for category Sex (Use
		// IsSelected method)
		// Storing all the elements under category 'Sex' in the list of WebLements
		List<WebElement> rdBtn_Sex = driver.findElements(By.name("sex"));

		// Create a boolean variable which will hold the value (True/False)
		boolean bValue = false;

		// This statement will return True, in case of first Radio button is selected
		bValue = rdBtn_Sex.get(0).isSelected();

		// This will check that if the bValue is True means if the first radio button is
		// selected
		if (bValue == true) {
			// This will select Second radio button, if the first radio button is selected
			// by default
			rdBtn_Sex.get(1).click();
		} else {
			// If the first radio button is not selected by default, the first will be
			// selected
			rdBtn_Sex.get(0).click();
		}
		System.out.println("Selecting  Gender  ....female ");
		driver.findElement(By.xpath("//*[@id=\"u_0_11\"]")).click();//
		Thread.sleep(8000);
		System.out.println("Submiting  FB Sign up form .... ");
		System.out.println("Now closing the driver .... ");
		driver.close();

	}
}