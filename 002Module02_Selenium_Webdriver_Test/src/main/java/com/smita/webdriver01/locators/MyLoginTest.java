/**
 * First Selenium WebDriver Test Case
 */
package com.smita.webdriver01.locators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.smita.webdriver.util.ChromeDriverUtil;
/**
 * @author Smita B Kumar
 *
 */
public class MyLoginTest {
	public static void main(String[] args) {
		//Step 1: Launch the Chrome browser	
		WebDriver driver=ChromeDriverUtil.getDriver();
		driver.get("http://www.gcrit.com/build3/admin/login.php");
		//Step 3: Finding element by name and sending the key i.e input data
		driver.findElement(By.name("username")).sendKeys("admin");
		driver.findElement(By.name("password")).sendKeys("admin@12svc3");
		//Step 4: Finding element by id i.e button click
		driver.findElement(By.id("tdb1")).click();
		
		//Step 5: getting the current Url
		String strUrl=driver.getCurrentUrl();
		System.out.println("The Current Url is : "+strUrl);
		//Step 6 :checking the current url with the expected
		if(strUrl.equals("http://www.gcrit.com/build3/admin/index.php")){
			System.out.println("Login successfull !!Test Case Pass");
		}else{
			System.err.println("Login unsuccessfull !!Test Case Failed");
		}
		//Step : close the browser
		//driver.close();//close the browser
	}
}
