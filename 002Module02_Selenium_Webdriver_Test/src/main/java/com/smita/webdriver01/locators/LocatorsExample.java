package com.smita.webdriver01.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.smita.webdriver.util.ChromeDriverUtil;

public class LocatorsExample {

	public static void main(String[] args) throws InterruptedException
	{
		WebDriver driver=ChromeDriverUtil.getDriver();

		driver.get("file:///E://JEE_WS//060SeleniumWebDriver//src//Locators.html");
		Thread.sleep(5000);
		//by id
		WebElement item = driver.findElement(By.id("user"));
		//by name
		WebElement locator = driver.findElement(By.name("admin"));		
		//by link
		item = driver.findElement(By.linkText("How to use locators?"));
	}

}
