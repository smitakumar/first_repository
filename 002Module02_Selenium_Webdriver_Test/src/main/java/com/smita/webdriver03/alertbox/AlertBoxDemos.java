package com.smita.webdriver03.alertbox;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smita.webdriver.util.ChromeDriverUtil;

public class AlertBoxDemos
{

	public static void main(String[] args) throws InterruptedException
	{
		
		WebDriver driver=ChromeDriverUtil.getDriver();

		driver.get("file:///E:/Syntel/WS/060SeleniumWebDriver/src/AlertBoxDemos.html");
		
		Thread.sleep(500);

		driver.findElement(By.id("alert")).click();
		
		Thread.sleep(500);
		
		Alert alert=driver.switchTo().alert();
		System.out.println("The alert message is : " + alert.getText());
		alert.accept();
		
		Thread.sleep(500);
		
		driver.findElement(By.id("confirm")).click();
		
		Thread.sleep(500);
		
		Alert confirm=driver.switchTo().alert();
		
		confirm.accept();
		
		Thread.sleep(500);
		
		driver.findElement(By.id("confirm")).click();
		
		Thread.sleep(500);
		
		confirm=driver.switchTo().alert();
		
		confirm.dismiss();
		
		Thread.sleep(500);
		
		driver.findElement(By.id("prompt")).click();
		
		Thread.sleep(500);
		
		Alert prompt=driver.switchTo().alert();
		
		String text=prompt.getText();
		System.out.println(text);
		
		prompt.sendKeys("Smith");
		
		Thread.sleep(500);
		
		prompt.accept();
		
		Thread.sleep(500);
		
		driver.findElement(By.id("prompt")).click();
		
		Thread.sleep(5000);
		
		/*prompt=driver.switchTo().alert();
		
		prompt.dismiss();*/
		
		Thread.sleep(500);
		
		driver.quit();
	}

}