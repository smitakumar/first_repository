package com.smita.webdriver05.explicit;
import java.time.LocalDateTime;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.smita.webdriver.util.ChromeDriverUtil;

public class ExplicitDemo01 
{

	public static void main(String[] args) throws InterruptedException 
	{
		LocalDateTime startdate=LocalDateTime.now();
		WebDriver driver=ChromeDriverUtil.getDriver();

		driver.get("http://demo.opencart.com/");

		WebDriverWait wait=new WebDriverWait(driver, 10);
		
		WebElement searchBox=wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.name("search")));
		
		searchBox.sendKeys("Phone");
		
		wait.until(
				ExpectedConditions.elementToBeClickable(
						By.className("input-group-btn"))).click();
		
		driver.quit();
		
		LocalDateTime enddate=LocalDateTime.now();
		System.out.println(startdate.toString());
		System.out.println(enddate.toString());

	}

}
