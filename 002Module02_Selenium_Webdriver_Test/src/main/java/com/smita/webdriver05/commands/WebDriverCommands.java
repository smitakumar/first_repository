package com.smita.webdriver05.commands;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.smita.webdriver.util.ChromeDriverUtil;
/**
 * @author Smita
 *
 */
public class WebDriverCommands {	 
	public static void main(String[] args) {
		// Create a new instance of the FireFox driver
		WebDriver driver = ChromeDriverUtil.getDriver(); 
		// Storing the Application Url in the String variable
		String url = "https://www.syntelinc.com/"; 
		//Launch the ToolsQA WebSite
		driver.get(url); 
		// Storing Title name in the String variable
		String title = driver.getTitle(); 
		// Storing Title length in the Int variable
		int titleLength = driver.getTitle().length(); 
		// Printing Title & Title length in the Console window
		System.out.println("Title of the page is : " + title);
		System.out.println("Length of the title is : "+ titleLength); 
		// Storing URL in String variable
		String actualUrl = driver.getCurrentUrl().toString(); 
		if (actualUrl.equals(url)){
			System.out.println("Verification Successful - The correct Url is opened.");
		}else{
			System.out.println("Verification Failed - An incorrect Url is opened.");
			//In case of Fail, you like to print the actual and expected URL for the record purpose
			System.out.println("Actual URL is : " + actualUrl);
			System.out.println("Expected URL is : " + url);
		} 
		// Storing Page Source in String variable
		String pageSource = driver.getPageSource(); 
		// Storing Page Source length in Int variable
		int pageSourceLength = pageSource.length(); 
		// Printing length of the Page Source on console
		System.out.println("Total length of the Pgae Source is : " + pageSourceLength);
		/*Use this statement to click on a New Window button "driver.findElement(By.xpath(".//*[@id=’tabs-1′]/div/p/a�?)).click();�?
		 * */
		driver.get("http://demoqa.com/frames-and-windows/");
		driver.findElement(By.xpath(".//*[@id='tabs-1']/div/p/a")).click();
		//Closing browser
		driver.quit();
	}
}