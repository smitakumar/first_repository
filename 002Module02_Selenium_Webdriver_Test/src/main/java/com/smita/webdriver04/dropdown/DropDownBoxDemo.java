package com.smita.webdriver04.dropdown;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.smita.webdriver.util.ChromeDriverUtil;

public class DropDownBoxDemo
{/*
Scenario to be automated
Launch the web browser and open the webpage
Click on the "Google� hyperlink
Navigate back to the original web page
Select the "Green� in colour dropdown
Select the "Orange� in the fruit dropdown
Select the "Elephant� in the animal dropdown
*/
	WebDriver driver;
    /**Set up browser settings and open the application     */
    @BeforeClass
    public void setUp() {
           driver=ChromeDriverUtil.getDriver();          
//Opened the application
           driver.get("file:\\E:\\JEE_WS\\060SeleniumWebDriver\\src\\DropDown.html");
           driver.manage().window().maximize();
    }
    /**
     * Test to select the dropdown values
     * @throws InterruptedException
     */
    @Test
    public void testSelectFunctionality() throws InterruptedException { 
          
//Go to google
           driver.findElement(By.linkText("Google")).click();
          
//navigate back to previous webpage
           driver.navigate().back();
           Thread.sleep(1000);
          
//select the first operator using "select by value"
           Select selectByValue = new 
        		   Select(driver.findElement(By.id("SelectID_One")));
           selectByValue.selectByValue("greenvalue");
           Thread.sleep(1000);
          
//select the second dropdown using "select by visible text"
           Select selectByVisibleText = new 
        		   Select (driver.findElement(By.id("SelectID_Two")));
           selectByVisibleText.selectByVisibleText("Lime");
           Thread.sleep(1000);
          
//select the third dropdown using "select by index"
           Select selectByIndex = new 
        		   Select(driver.findElement(By.id("SelectID_Three")));
           selectByIndex.selectByIndex(2);
           Thread.sleep(1000); 
//select button to check confirm box
        driver.findElement(By.id("try")).click();
        Alert confirm=driver.switchTo().alert();		
		confirm.accept();		
		Thread.sleep(1000);		
		driver.findElement(By.id("try")).click();		
		Thread.sleep(1000);		
		confirm=driver.switchTo().alert();		
		confirm.dismiss();		
		Thread.sleep(1000);
    }

    /**
     * Tear down the setup after test completes
     */

    @AfterClass
    public void tearDown() { 
           driver.quit();
    }
}
