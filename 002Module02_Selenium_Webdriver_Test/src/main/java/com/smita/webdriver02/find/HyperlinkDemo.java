package com.smita.webdriver02.find;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smita.webdriver.util.ChromeDriverUtil;

public class HyperlinkDemo{
	public static void main(String[] args) throws InterruptedException	{
		WebDriver driver = ChromeDriverUtil.getDriver();
		String baseURL = 
"file:///E:/Selenium/SeleniumWS_2017/060SeleniumWebDriver/src/HyperlinkDemo.html";
		
		driver.get(baseURL);
		driver.manage().window().maximize();
		
		//Using linkText locator
		driver.findElement(By.linkText("CLICK HERE!")).click();
		Thread.sleep(5000);
		
		driver.navigate().back();
		
		//Using partialLinkText locator
		driver.findElement(By.partialLinkText("ME")).click();

		System.out.println("The page title is : " + driver.getTitle());
		Thread.sleep(5000);
		driver.quit();
	}

}
