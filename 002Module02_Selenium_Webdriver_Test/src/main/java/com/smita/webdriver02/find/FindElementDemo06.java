package com.smita.webdriver02.find;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smita.webdriver.util.ChromeDriverUtil;

public class FindElementDemo06{

	public static void main(String[] args) throws InterruptedException
	{	
		WebDriver driver = ChromeDriverUtil.getDriver();
		driver.get("http://demo.opencart.com/");		
		Thread.sleep(10000);
////*[@id="search"]/span/button/i
		//Featured xpath
		String text=driver.findElement(
				By.xpath("//*[@id='content']/h3")).getText();
		
		System.out.println("Text : "+text);
		
		System.out.println("\n**************\n\nDiv text:");
		
		//1st Featured div
		System.out.println(driver.findElement(
				By.xpath("//*[@id='content']/div[2]/div[1]/div")).getText());		
		driver.quit();

	}
}