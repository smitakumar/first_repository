package com.smita.webdriver02.find;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smita.webdriver.util.ChromeDriverUtil;

public class FindElementDemo05 {

	public static void main(String[] args) throws InterruptedException {

			WebDriver driver = ChromeDriverUtil.getDriver();

		driver.get("http://demo.opencart.com/");

		Thread.sleep(3000);

		WebElement searchBox = driver.findElement(By.name("search"));

		searchBox.sendKeys("Phone");

		Thread.sleep(3000);

		driver.findElement(By.className("input-group-btn")).click();

		Thread.sleep(3000);

		driver.navigate().back();

		Thread.sleep(3000);
		driver.findElement(By.name("search")).clear();
		driver.findElement(By.name("search")).sendKeys("Mac");

		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='search']/span/button")).sendKeys(Keys.ENTER);

		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='search']/span/button")).sendKeys(Keys.PAGE_DOWN);
		Thread.sleep(2000);/*
		driver.findElement(By.name("search")).sendKeys(Keys.PAGE_DOWN);
		Thread.sleep(2000);*/
		driver.findElement(By.name("search")).sendKeys(Keys.PAGE_UP);
		Thread.sleep(2000);
		searchBox = driver.findElement(By.name("search"));
		searchBox.clear();
		searchBox.sendKeys(Keys.chord(Keys.SHIFT, "p"));
		searchBox.sendKeys(Keys.chord("h"));
		searchBox.sendKeys(Keys.chord("one"));
		Thread.sleep(5000);
		searchBox.sendKeys(Keys.BACK_SPACE);
		searchBox.sendKeys(Keys.BACK_SPACE);
		searchBox.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(3000);
		searchBox.clear();
		//accessing numpad
		searchBox.sendKeys(Keys.NUMPAD0);
		searchBox.sendKeys(Keys.NUMPAD1);
		searchBox.sendKeys(Keys.NUMPAD2);
		searchBox.sendKeys(Keys.NUMPAD3);
		Thread.sleep(3000);
		searchBox.clear();
		//enabling fullscreen
		searchBox.sendKeys(Keys.chord(Keys.F11));
		Thread.sleep(10000);
		//disabling fullscreen
		searchBox.sendKeys(Keys.F11);

		Thread.sleep(5000);
//refresh
		searchBox.sendKeys(Keys.F5);

		Thread.sleep(3000);

		driver.quit();

	}

}
