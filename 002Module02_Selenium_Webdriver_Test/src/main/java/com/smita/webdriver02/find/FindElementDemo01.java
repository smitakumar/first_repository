package com.smita.webdriver02.find;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.smita.webdriver.util.ChromeDriverUtil;

public class FindElementDemo01 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeDriverUtil.getDriver();
		driver.get("http://demo.opencart.com/");

		Thread.sleep(3000);

		WebElement searchBox = driver.findElement(By.name("search"));

		/* WebElement searchBox=driver.findElement(
		By.className("form-control input-lg"));*/
		searchBox.sendKeys("Phone");
		Thread.sleep(5000);
		driver.quit();
	}
}