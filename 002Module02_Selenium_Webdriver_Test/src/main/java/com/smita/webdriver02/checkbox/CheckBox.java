package com.smita.webdriver02.checkbox;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.smita.webdriver.util.ChromeDriverUtil;

public class CheckBox {
	WebDriver driver;

	/** Set up browser settings and open the application */
	@BeforeClass
	public void setUp() {
		driver = ChromeDriverUtil.getDriver();
		// Opened the application
		driver.get("file:///E:/Syntel/WS/060SeleniumWebDriver/src/CheckBoxDemo.html");
		driver.manage().window();
	}

	@Test
	public void testSelectFunctionality() throws InterruptedException {
		/*
		 * String checkBoxVal = ""; WebElement checkBox =
		 * driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/input[1]"));
		 * checkBox.click(); Thread.sleep(2000); checkBox =
		 * driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/input[2]"));
		 * checkBox.click(); Thread.sleep(2000); checkBox =
		 * driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/input[3]"));
		 * checkBox.click(); Thread.sleep(2000); checkBox =
		 * driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[2]/input[4]"));
		 * checkBox.click(); Thread.sleep(2000);
		 */

		// Find the checkbox or radio button element by its name.
		List<WebElement> list = driver.findElements(By.name("chk1"));

		// Get the number of checkboxes available.
		int count = list.size();
		String checkBoxValue = "";
		// Now, iterate the loop from first checkbox to last checkbox.
		for (int i = 0; i < count; i++) {
			list.get(i).click();
			Thread.sleep(2000);
			// Store the checkbox name to the string variable, using 'Value'
			// attribute
			checkBoxValue += list.get(i).getAttribute("value") + ", ";
			Thread.sleep(2000);			
		}
		System.out.println(checkBoxValue);
		Thread.sleep(5000);
		if (driver != null)
			driver.close();
	}
}
