package com.smita.webdriver02.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.smita.webdriver.util.ChromeDriverUtil;

public class SubmitForm {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeDriverUtil.getDriver();
		driver.get("http://demo.opencart.com/index.php?route=account/register");
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"input-firstname\"]")).sendKeys("smita");
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"input-lastname\"]")).sendKeys("kumar");
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys("smitakumar00@gmail.com");
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"input-telephone\"]")).sendKeys("982246789");
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys("smita@123");;
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"input-confirm\"]")).sendKeys("smita@123");
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset[3]/div/div/label[1]/input")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[1]")).click();
		Thread.sleep(3000);
		
		
		driver.findElement(By.cssSelector("input[type=submit]")).click();
		Thread.sleep(3000);
		
		driver.quit();
		
		
}
}